
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt
import os


# In[33]:

get_ipython().magic(u'matplotlib')


# In[11]:

data = np.loadtxt("hw1.2-data.txt")

#### Vector Quantization ###


#### Pick the initial clusters ####

cluster_indices = random.choice(xrange(100), 3) #Pick 3 indices at random
centroids = data[np.transpose(cluster_indices)]

#### euclidean distance ####
def eucl_dist(a, b):
    return np.linalg.norm(a-b)

#### Calculate closest centroid ####
def closest_centroid(a, clusters):
    dists = [eucl_dist(a, x) for x in clusters]
    return np.argmin(dists)

#### Calculate cluster memberships ####
def cluster_memberships(clusters):
    member_list = []
    for i in xrange(100):
        member_list.append(closest_centroid(data[i], clusters))
    return member_list

#### Compute new centroids ####
def compute_centroids(member_list):
    cluster_members = np.asarray(member_list)
    #cluster_data = np.hstack(data, cluster_column) #Adds membership info to the data
    clusters = np.empty([3,2])
    for i in range(3):
        partioned_data = data[np.where(cluster_members == i)]
        centroid = np.average(partioned_data, axis = 0)
        clusters[i] = centroid
    #print "The new temp cluster is "
    #print clusters
    return clusters    

cluster_membership = cluster_memberships(centroids)
reassigned_flag = True

max_iter = 1000 #Maximum iterations of while loop
iter_count = 1
member_list = cluster_membership

#Keep re-calculating clusters until no more change in membership
while(reassigned_flag):
    if iter_count > max_iter:
        print "Exceeded max iterations"
        break
    iter_count += 1
    cluster_membership = member_list
    centroids = compute_centroids(member_list)
    #print centroids
    member_list = cluster_memberships(centroids)
    if cluster_membership == member_list:
        break

zero = data[np.where(np.asarray(cluster_membership) == 0)]
one = data[np.where(np.asarray(cluster_membership) == 1)]
two = data[np.where(np.asarray(cluster_membership) == 2)]
print iter_count


# In[9]:

color = []
for member in cluster_membership:
    if member == 0:
        color.append('r')
    elif member == 1:
        color.append('b')
    else:
        color.append('g')
plt.scatter(data[:,0], data[:,1], marker = '+', c = color)
plt.scatter(centroids[:,0], centroids[:,1], marker = 'o', s = 50, c = ['r', 'b', 'g'])

for i in range(3):
    txt = str(round(centroids[:,0][i], 2)) + "," + str(round(centroids[:,1][i], 2))
    plt.annotate(txt, (centroids[:,0][i], centroids[:,1][i]))

