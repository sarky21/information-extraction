import tables, hashes
import sequtils
import math
import strutils
import times


################## Step 1 - Create elementary fenonic models ###########################

#Label names are a natural string sequence with ordering going from 0 to 255 defined by 
# 26*(ord(c1) - 65) + (ord(c2) - 65)

#Create a fenone type. This will simply be a tuple of probabilities
type
    fenone = tuple[p: seq[float], q: seq[seq[float]]]    #Each q is an array of length 255 

var fenone_list = newSeq[fenone](256)  #Hashmap that maps label to fenone

#Initialize all the fenone HMM's

for i in 0..255:
    var elementary_fenone: fenone
    elementary_fenone.p = @[0.8, 0.1, 0.1]

    let q_prob = 0.5/255
    var temp_q = newSeqWith(2, newSeqWith(256, q_prob))

    #Set the actual output values as 0.5
    temp_q[0][i] = 0.5
    temp_q[1][i] = 0.5

    elementary_fenone.q = temp_q
    #Append the elementary fenone to fenone_list
    fenone_list[i] = elementary_fenone


####################### Step 2 - Create a silence model ########################


var silence: fenone
silence.p = newSeqWith(12, 0.5)   #p1 to p9 are the output producing arcs. p10 to p12 are the null arcs
let sil_output_prob = 1/256
silence.q = newSeqWith(9, newSeqWith(256, sil_output_prob))

####################### Step 3 - Pick fenonic baseforms ########################

#Load the training script into memory
var scriptSeq:seq[string] = @[]

var f = open("data.2015/clsp.trnscr")

var count = 0
for line in f.lines:
    case count
    of 0: discard
    else:
        scriptSeq.add(line)
    count += 1
f.close()

#Define the baseform type. Baseform is just a list of integers referring to position of the fenones
type
    fenonic_baseform = seq[int]

#Load the endpoint information into a sequence
var endPtSeq = newSeqWith(798, @[0, 0])

f = open("data.2015/clsp.endpts")
count = 0
for line in f.lines:
    case count
    of 0: discard
    else:
        var silEnd:int = parseInt(line.split(" ")[0])  #End of the initial silence
        var silStart:int = parseInt(line.split(" ")[1])  #Start of the trailing silence
        endPtSeq[count-1] = @[silEnd, silStart]       
    count += 1
f.close()

#Load the training data, and identify the baseforms

var baseforms = newTable[string, fenonic_baseform]()  #create a string, HMM hashmap

var trnlbls:seq[string] = @[]

f = open("data.2015/clsp.trnlbls")
count = 0
for line in f.lines:
    case count
    of 0: discard
    else:
        var line_stripped = line.strip()
        trnlbls.add(line_stripped)
        var labels = line_stripped.split(" ") 
        var startPt:int = endPtSeq[count-1][0]
        var endPt:int = endPtSeq[count-1][1] - 2 #The file is 1 indexed and the value stored is the start of trailing silence
        var key:string = scriptSeq[count-1]

        var labels_length = endPt - startPt + 1

        #Only use first instance of word for baseform
        if baseforms.hasKey(key):
            continue
        else:
            var tempSeq = newSeq[int](labels_length)
            for i in startPt .. endPt:
                var lbl:string = labels[i]
                var lbl_index = 26*(ord(lbl[0]) - 65) + (ord(lbl[1]) - 65)
                tempSeq[i-startPt] = lbl_index
            baseforms[key] = tempSeq
    count += 1
f.close()


####################### Step 4 - Training the word HMM's ########################

var num_iters = 1 ######### Number of iterations 

for iter in 0 .. num_iters:
    #For each iteration do forward-backward over each utterance and then update probabilities

    ############### TODO Define all the count variables here ###################


    for sample_num, sample in trnlbls:
        #forward-backward for each sample
        #Initial start state is s0

        var key = scriptSeq[sample_num]
        var baseform = baseforms[key]
        var labels = sample.split(" ")
        var obs_len = len(labels)


        ##################### Forward backward over the joint HMM ###################
        #Initialize alphas for silence model. All alphas and betas are sequences of sequences of length num_states X len(obs_seq)

        var num_states = 7 + len(baseform)*2 + 7

        var alpha_hat = newSeqWith(obs_len+1, newSeqWith(num_states, 0.0))
        var alpha_star = newSeqWith(obs_len+1, newSeqWith(num_states, 0.0))
        var norm_seq = newSeq[float](obs_len+1)

        alpha_star[0][0] = 1.0 #Start state is always the first state

        norm_seq[0] = 1.0

        echo len(alpha_hat)

        for stage, obs in labels:
            var obs_index = 26*(ord(obs[0]) - 65) + (ord(obs[1]) - 65)
            var stage_index = stage + 1
            var current_q = 0.0
            var temp_alpha_hat = 0.0

            #Hardcoding the alpha update equations. Seems more efficient speed wise. Weird
            temp_alpha_hat = alpha_star[stage_index-1][1]*silence.p[1]*silence.q[1][obs_index] +
                             alpha_star[stage_index-1][0]*silence.p[0]*silence.q[0][obs_index]  
            alpha_hat[stage_index][1] = temp_alpha_hat    #alpha(2)
            current_q += temp_alpha_hat

            temp_alpha_hat = alpha_star[stage_index-1][2]*silence.p[3]*silence.q[3][obs_index] + 
                             alpha_star[stage_index-1][1]*silence.p[2]*silence.q[2][obs_index]
            alpha_hat[stage_index][2] = temp_alpha_hat    #alpha(3)
            current_q += temp_alpha_hat

            temp_alpha_hat = alpha_star[stage_index-1][0]*silence.p[5]*silence.q[5][obs_index]
            alpha_hat[stage_index][3] = temp_alpha_hat    #alpha(4)
            current_q += temp_alpha_hat

            temp_alpha_hat = alpha_star[stage_index-1][3]*silence.p[6]*silence.q[6][obs_index]
            alpha_hat[stage_index][4] = temp_alpha_hat    #alpha(5)
            current_q += temp_alpha_hat

            temp_alpha_hat = alpha_star[stage_index-1][4]*silence.p[7]*silence.q[7][obs_index]
            alpha_hat[stage_index][5] = temp_alpha_hat    #alpha(6)
            current_q += temp_alpha_hat

            temp_alpha_hat = alpha_star[stage_index-1][2]*silence.p[4]*silence.q[4][obs_index] + 
                             alpha_hat[stage_index][3]*silence.p[9] + alpha_hat[stage_index][4]*silence.p[10] +
                             alpha_hat[stage_index][5]*silence.p[11] + alpha_star[stage_index-1][5]*silence.p[8]*silence.q[8][obs_index]

            alpha_hat[stage_index][6] = temp_alpha_hat    #alpha(7)
            current_q += temp_alpha_hat

            #Forward run over the 

        ######### Backward run ###############

        var beta_hat_sil = newSeqWith(obs_len+1, newSeqWith(7, 0.0))
        var beta_star_sil = newSeqWith(obs_len+1, newSeqWith(7, 0.0))

        for i in countdown(obs_len-1, 0):
            var obs = labels[i]
            var obs_index =  26*(ord(obs[0]) - 65) + (ord(obs[1]) - 65)
            var stage_index = i




echo "Phew it compiled"
