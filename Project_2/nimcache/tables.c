/* Generated by Nim Compiler v0.10.2 */
/*   (c) 2014 Andreas Rumpf */
/* The generated code is subject to the original license. */
/* Compiled for: Linux, amd64, gcc */
/* Command for C compiler:
   gcc -c  -w -O3 -fno-strict-aliasing  -I/home/freeman/Code/nim-0.10.2/lib -o /home/freeman/Courses/Information_Extraction/Project_2/nimcache/tables.o /home/freeman/Courses/Information_Extraction/Project_2/nimcache/tables.c */
#define NIM_INTBITS 64
#include "nimbase.h"

#include <string.h>
typedef struct table113814 table113814;
typedef struct keyvaluepairseq113817 keyvaluepairseq113817;
typedef struct keyvaluepair113820 keyvaluepair113820;
typedef struct NimStringDesc NimStringDesc;
typedef struct TGenericSeq TGenericSeq;
typedef struct TY113639 TY113639;
typedef struct TNimType TNimType;
typedef struct TNimNode TNimNode;
typedef struct tcell44935 tcell44935;
typedef struct tcellseq44951 tcellseq44951;
typedef struct tgcheap47016 tgcheap47016;
typedef struct tcellset44947 tcellset44947;
typedef struct tpagedesc44943 tpagedesc44943;
typedef struct tmemregion27210 tmemregion27210;
typedef struct tsmallchunk26440 tsmallchunk26440;
typedef struct tllchunk27204 tllchunk27204;
typedef struct tbigchunk26442 tbigchunk26442;
typedef struct tintset26417 tintset26417;
typedef struct ttrunk26413 ttrunk26413;
typedef struct tavlnode27208 tavlnode27208;
typedef struct tgcstat47014 tgcstat47014;
typedef struct table114767 table114767;
typedef struct keyvaluepairseq114770 keyvaluepairseq114770;
typedef struct keyvaluepair114773 keyvaluepair114773;
typedef struct hmm114724 hmm114724;
typedef struct state114722 state114722;
typedef struct TY114733 TY114733;
typedef struct arc114718 arc114718;
typedef struct tbasechunk26438 tbasechunk26438;
typedef struct tfreecell26430 tfreecell26430;
struct  TGenericSeq  {
NI len;
NI reserved;
};
struct  NimStringDesc  {
  TGenericSeq Sup;
NIM_CHAR data[SEQ_DECL_SIZE];
};
struct keyvaluepair113820 {
NU8 Field0;
NimStringDesc* Field1;
TY113639* Field2;
};
struct  table113814  {
keyvaluepairseq113817* Data;
NI Counter;
};
typedef N_NIMCALL_PTR(void, TY3089) (void* p, NI op);
typedef N_NIMCALL_PTR(void*, TY3094) (void* p);
struct  TNimType  {
NI size;
NU8 kind;
NU8 flags;
TNimType* base;
TNimNode* node;
void* finalizer;
TY3089 marker;
TY3094 deepcopy;
};
struct  TNimNode  {
NU8 kind;
NI offset;
TNimType* typ;
NCSTRING name;
NI len;
TNimNode** sons;
};
struct  tcell44935  {
NI Refcount;
TNimType* Typ;
};
struct  tcellseq44951  {
NI Len;
NI Cap;
tcell44935** D;
};
struct  tcellset44947  {
NI Counter;
NI Max;
tpagedesc44943* Head;
tpagedesc44943** Data;
};
typedef tsmallchunk26440* TY27222[512];
typedef ttrunk26413* ttrunkbuckets26415[256];
struct  tintset26417  {
ttrunkbuckets26415 Data;
};
struct  tmemregion27210  {
NI Minlargeobj;
NI Maxlargeobj;
TY27222 Freesmallchunks;
tllchunk27204* Llmem;
NI Currmem;
NI Maxmem;
NI Freemem;
NI Lastsize;
tbigchunk26442* Freechunkslist;
tintset26417 Chunkstarts;
tavlnode27208* Root;
tavlnode27208* Deleted;
tavlnode27208* Last;
tavlnode27208* Freeavlnodes;
};
struct  tgcstat47014  {
NI Stackscans;
NI Cyclecollections;
NI Maxthreshold;
NI Maxstacksize;
NI Maxstackcells;
NI Cycletablesize;
NI64 Maxpause;
};
struct  tgcheap47016  {
void* Stackbottom;
NI Cyclethreshold;
tcellseq44951 Zct;
tcellseq44951 Decstack;
tcellset44947 Cycleroots;
tcellseq44951 Tempstack;
NI Recgclock;
tmemregion27210 Region;
tgcstat47014 Stat;
};
struct arc114718 {
NI Field0;
NI Field1;
NI Field2;
NI Field3;
};
struct state114722 {
TY114733* Field0;
TY114733* Field1;
TY114733* Field2;
TY114733* Field3;
};
struct keyvaluepair114773 {
NU8 Field0;
NimStringDesc* Field1;
hmm114724* Field2;
};
struct  table114767  {
keyvaluepairseq114770* Data;
NI Counter;
};
typedef NI TY26420[8];
struct  tpagedesc44943  {
tpagedesc44943* Next;
NI Key;
TY26420 Bits;
};
struct  tbasechunk26438  {
NI Prevsize;
NI Size;
NIM_BOOL Used;
};
struct  tsmallchunk26440  {
  tbasechunk26438 Sup;
tsmallchunk26440* Next;
tsmallchunk26440* Prev;
tfreecell26430* Freelist;
NI Free;
NI Acc;
NF Data;
};
struct  tllchunk27204  {
NI Size;
NI Acc;
tllchunk27204* Next;
};
struct  tbigchunk26442  {
  tbasechunk26438 Sup;
tbigchunk26442* Next;
tbigchunk26442* Prev;
NI Align;
NF Data;
};
struct  ttrunk26413  {
ttrunk26413* Next;
NI Key;
TY26420 Bits;
};
typedef tavlnode27208* TY27214[2];
struct  tavlnode27208  {
TY27214 Link;
NI Key;
NI Upperbound;
NI Level;
};
struct  tfreecell26430  {
tfreecell26430* Next;
NI Zerofield;
};
struct TY113639 {
  TGenericSeq Sup;
  NI data[SEQ_DECL_SIZE];
};
struct keyvaluepairseq113817 {
  TGenericSeq Sup;
  keyvaluepair113820 data[SEQ_DECL_SIZE];
};
struct TY114733 {
  TGenericSeq Sup;
  arc114718 data[SEQ_DECL_SIZE];
};
struct hmm114724 {
  TGenericSeq Sup;
  state114722 data[SEQ_DECL_SIZE];
};
struct keyvaluepairseq114770 {
  TGenericSeq Sup;
  keyvaluepair114773 data[SEQ_DECL_SIZE];
};
N_NIMCALL(void, nimGCvisit)(void* d, NI op);
N_NIMCALL(void, TMP60)(void* p, NI op);
N_NIMCALL(void, TMP61)(void* p, NI op);
N_NIMCALL(void*, newObj)(TNimType* typ, NI size);
N_NIMCALL(void, inittable_113869)(NI initialsize_113874, table113814* Result);
N_NIMCALL(void*, newSeq)(TNimType* typ, NI len);
N_NIMCALL(void, unsureAsgnRef)(void** dest, void* src);
N_NIMCALL(NIM_BOOL, haskey_114491)(table113814 t_114498, NimStringDesc* key_114501);
N_NIMCALL(NI, rawget_114093)(table113814 t_114100, NimStringDesc* key_114103);
N_NIMCALL(NI, hash_98842)(NimStringDesc* x);
static N_INLINE(NIM_BOOL, eqStrings)(NimStringDesc* a, NimStringDesc* b);
static N_INLINE(NI, nexttry_108409)(NI h, NI maxhash);
N_NIMCALL(void, HEX5BHEX5DHEX3D_114075)(table113814* t_114084, NimStringDesc* key_114088, TY113639* val_114090);
N_NIMCALL(void, genericSeqAssign)(void* dest, void* src_79604, TNimType* mt);
static N_INLINE(NIM_BOOL, mustrehash_108302)(NI length, NI counter);
N_NIMCALL(void, enlarge_114150)(table113814* t_114159);
N_NIMCALL(void, rawinsert_114220)(table113814* t_114229, keyvaluepairseq113817** data_114237, NimStringDesc* key_114241, TY113639* val_114243);
N_NIMCALL(NimStringDesc*, copyStringRC1)(NimStringDesc* src);
static N_INLINE(void, nimGCunrefNoCycle)(void* p);
static N_INLINE(tcell44935*, usrtocell_48646)(void* usr);
static N_INLINE(void, rtladdzct_50204)(tcell44935* c);
N_NOINLINE(void, addzct_48617)(tcellseq44951* s, tcell44935* c);
N_NIMCALL(void, TMP69)(void* p, NI op);
N_NIMCALL(void, TMP70)(void* p, NI op);
N_NIMCALL(void, inittable_114822)(NI initialsize_114827, table114767* Result);
N_NIMCALL(void, HEX5BHEX5DHEX3D_115075)(table114767* t_115084, NimStringDesc* key_115088, hmm114724* val_115090);
N_NIMCALL(NI, rawget_115093)(table114767 t_115100, NimStringDesc* key_115103);
N_NIMCALL(void, enlarge_115150)(table114767* t_115159);
N_NIMCALL(void, rawinsert_115220)(table114767* t_115229, keyvaluepairseq114770** data_115237, NimStringDesc* key_115241, hmm114724* val_115243);
N_NIMCALL(hmm114724*, HEX5BHEX5D_117461)(table114767 t_117468, NimStringDesc* key_117471);
TNimType NTI113814; /* Table */
TNimType NTI113820; /* KeyValuePair */
TNimType NTI108009; /* SlotEnum */
extern TNimType NTI149; /* string */
extern TNimType NTI113639; /* seq[int] */
TNimType NTI113817; /* KeyValuePairSeq */
extern TNimType NTI108; /* int */
TNimType NTI113811; /* TableRef */
extern tgcheap47016 gch_47044;
TNimType NTI114767; /* Table */
TNimType NTI114773; /* KeyValuePair */
extern TNimType NTI114724; /* hmm */
TNimType NTI114770; /* KeyValuePairSeq */
TNimType NTI114764; /* TableRef */
N_NIMCALL(void, TMP60)(void* p, NI op) {
	keyvaluepairseq113817* a;
	NI LOC1;
	a = (keyvaluepairseq113817*)p;
	LOC1 = 0;
	for (LOC1 = 0; LOC1 < a->Sup.len; LOC1++) {
	nimGCvisit((void*)a->data[LOC1].Field1, op);
	nimGCvisit((void*)a->data[LOC1].Field2, op);
	}
}
N_NIMCALL(void, TMP61)(void* p, NI op) {
	table113814* a;
	a = (table113814*)p;
	nimGCvisit((void*)(*a).Data, op);
}

N_NIMCALL(void, inittable_113869)(NI initialsize_113874, table113814* Result) {
	(*Result).Counter = 0;
	unsureAsgnRef((void**) (&(*Result).Data), (keyvaluepairseq113817*) newSeq((&NTI113817), initialsize_113874));
}

N_NIMCALL(table113814*, newtable_114368)(NI initialsize_114373) {
	table113814* result;
	result = 0;
	result = (table113814*) newObj((&NTI113811), sizeof(table113814));
	inittable_113869(initialsize_114373, (&(*result)));
	return result;
}

static N_INLINE(NIM_BOOL, eqStrings)(NimStringDesc* a, NimStringDesc* b) {
	NIM_BOOL result;
	NIM_BOOL LOC11;
	int LOC13;
	result = 0;
	{
		if (!(a == b)) goto LA3;
		result = NIM_TRUE;
		goto BeforeRet;
	}
	LA3: ;
	{
		NIM_BOOL LOC7;
		LOC7 = 0;
		LOC7 = (a == NIM_NIL);
		if (LOC7) goto LA8;
		LOC7 = (b == NIM_NIL);
		LA8: ;
		if (!LOC7) goto LA9;
		result = NIM_FALSE;
		goto BeforeRet;
	}
	LA9: ;
	LOC11 = 0;
	LOC11 = ((*a).Sup.len == (*b).Sup.len);
	if (!(LOC11)) goto LA12;
	LOC13 = 0;
	LOC13 = memcmp(((NCSTRING) ((*a).data)), ((NCSTRING) ((*b).data)), (NI64)((*a).Sup.len * 1));
	LOC11 = (LOC13 == ((NI32) 0));
	LA12: ;
	result = LOC11;
	goto BeforeRet;
	BeforeRet: ;
	return result;
}

static N_INLINE(NI, nexttry_108409)(NI h, NI maxhash) {
	NI result;
	result = 0;
	result = (NI)((NI64)((NI64)(5 * h) + 1) & maxhash);
	return result;
}

N_NIMCALL(NI, rawget_114093)(table113814 t_114100, NimStringDesc* key_114103) {
	NI result;
	NI h;
	NI LOC1;
	result = 0;
	LOC1 = 0;
	LOC1 = hash_98842(key_114103);
	h = (NI)(LOC1 & (t_114100.Data->Sup.len-1));
	{
		while (1) {
			if (!!((t_114100.Data->data[h].Field0 == ((NU8) 0)))) goto LA3;
			{
				NIM_BOOL LOC6;
				LOC6 = 0;
				LOC6 = eqStrings(t_114100.Data->data[h].Field1, key_114103);
				if (!(LOC6)) goto LA7;
				LOC6 = (t_114100.Data->data[h].Field0 == ((NU8) 1));
				LA7: ;
				if (!LOC6) goto LA8;
				result = h;
				goto BeforeRet;
			}
			LA8: ;
			h = nexttry_108409(h, (t_114100.Data->Sup.len-1));
		} LA3: ;
	}
	result = -1;
	BeforeRet: ;
	return result;
}

N_NIMCALL(NIM_BOOL, haskey_114491)(table113814 t_114498, NimStringDesc* key_114501) {
	NIM_BOOL result;
	NI LOC1;
	result = 0;
	LOC1 = 0;
	LOC1 = rawget_114093(t_114498, key_114501);
	result = (0 <= LOC1);
	return result;
}

N_NIMCALL(NIM_BOOL, haskey_114477)(table113814* t_114484, NimStringDesc* key_114487) {
	NIM_BOOL result;
	result = 0;
	result = haskey_114491((*t_114484), key_114487);
	return result;
}

static N_INLINE(NIM_BOOL, mustrehash_108302)(NI length, NI counter) {
	NIM_BOOL result;
	NIM_BOOL LOC1;
	result = 0;
	LOC1 = 0;
	LOC1 = ((NI64)(length * 2) < (NI64)(counter * 3));
	if (LOC1) goto LA2;
	LOC1 = ((NI64)(length - counter) < 4);
	LA2: ;
	result = LOC1;
	return result;
}

static N_INLINE(tcell44935*, usrtocell_48646)(void* usr) {
	tcell44935* result;
	result = 0;
	result = ((tcell44935*) ((NI)((NU64)(((NI) (usr))) - (NU64)(((NI)sizeof(tcell44935))))));
	return result;
}

static N_INLINE(void, rtladdzct_50204)(tcell44935* c) {
	addzct_48617((&gch_47044.Zct), c);
}

static N_INLINE(void, nimGCunrefNoCycle)(void* p) {
	tcell44935* c;
	c = usrtocell_48646(p);
	{
		(*c).Refcount -= 8;
		if (!((NU64)((*c).Refcount) < (NU64)(8))) goto LA3;
		rtladdzct_50204(c);
	}
	LA3: ;
}

N_NIMCALL(void, rawinsert_114220)(table113814* t_114229, keyvaluepairseq113817** data_114237, NimStringDesc* key_114241, TY113639* val_114243) {
	NI h;
	NI LOC1;
	NimStringDesc* LOC4;
	LOC1 = 0;
	LOC1 = hash_98842(key_114241);
	h = (NI)(LOC1 & ((*data_114237)->Sup.len-1));
	{
		while (1) {
			if (!((*data_114237)->data[h].Field0 == ((NU8) 1))) goto LA3;
			h = nexttry_108409(h, ((*data_114237)->Sup.len-1));
		} LA3: ;
	}
	LOC4 = 0;
	LOC4 = (*data_114237)->data[h].Field1; (*data_114237)->data[h].Field1 = copyStringRC1(key_114241);
	if (LOC4) nimGCunrefNoCycle(LOC4);
	genericSeqAssign((&(*data_114237)->data[h].Field2), val_114243, (&NTI113639));
	(*data_114237)->data[h].Field0 = ((NU8) 1);
}

N_NIMCALL(void, enlarge_114150)(table113814* t_114159) {
	keyvaluepairseq113817* n;
	keyvaluepairseq113817* LOC8;
	n = 0;
	n = (keyvaluepairseq113817*) newSeq((&NTI113817), (NI64)((*t_114159).Data->Sup.len * 2));
	{
		NI i_114205;
		NI HEX3Atmp_114284;
		NI res_114287;
		i_114205 = 0;
		HEX3Atmp_114284 = 0;
		HEX3Atmp_114284 = ((*t_114159).Data->Sup.len-1);
		res_114287 = 0;
		{
			while (1) {
				if (!(res_114287 <= HEX3Atmp_114284)) goto LA3;
				i_114205 = res_114287;
				{
					if (!((*t_114159).Data->data[i_114205].Field0 == ((NU8) 1))) goto LA6;
					rawinsert_114220(t_114159, (&n), (*t_114159).Data->data[i_114205].Field1, (*t_114159).Data->data[i_114205].Field2);
				}
				LA6: ;
				res_114287 += 1;
			} LA3: ;
		}
	}
	LOC8 = 0;
	LOC8 = (*t_114159).Data;
	unsureAsgnRef((void**) (&(*t_114159).Data), n);
	n = LOC8;
}

N_NIMCALL(void, HEX5BHEX5DHEX3D_114075)(table113814* t_114084, NimStringDesc* key_114088, TY113639* val_114090) {
	NI index;
	index = rawget_114093((*t_114084), key_114088);
	{
		if (!(0 <= index)) goto LA3;
		genericSeqAssign((&(*t_114084).Data->data[index].Field2), val_114090, (&NTI113639));
	}
	goto LA1;
	LA3: ;
	{
		{
			NIM_BOOL LOC8;
			LOC8 = 0;
			LOC8 = mustrehash_108302((*t_114084).Data->Sup.len, (*t_114084).Counter);
			if (!LOC8) goto LA9;
			enlarge_114150(t_114084);
		}
		LA9: ;
		rawinsert_114220(t_114084, (&(*t_114084).Data), key_114088, val_114090);
		(*t_114084).Counter += 1;
	}
	LA1: ;
}

N_NIMCALL(void, HEX5BHEX5DHEX3D_114624)(table113814* t_114631, NimStringDesc* key_114634, TY113639* val_114636) {
	HEX5BHEX5DHEX3D_114075(t_114631, key_114634, val_114636);
}
N_NIMCALL(void, TMP69)(void* p, NI op) {
	keyvaluepairseq114770* a;
	NI LOC1;
	a = (keyvaluepairseq114770*)p;
	LOC1 = 0;
	for (LOC1 = 0; LOC1 < a->Sup.len; LOC1++) {
	nimGCvisit((void*)a->data[LOC1].Field1, op);
	nimGCvisit((void*)a->data[LOC1].Field2, op);
	}
}
N_NIMCALL(void, TMP70)(void* p, NI op) {
	table114767* a;
	a = (table114767*)p;
	nimGCvisit((void*)(*a).Data, op);
}

N_NIMCALL(void, inittable_114822)(NI initialsize_114827, table114767* Result) {
	(*Result).Counter = 0;
	unsureAsgnRef((void**) (&(*Result).Data), (keyvaluepairseq114770*) newSeq((&NTI114770), initialsize_114827));
}

N_NIMCALL(table114767*, newtable_115368)(NI initialsize_115373) {
	table114767* result;
	result = 0;
	result = (table114767*) newObj((&NTI114764), sizeof(table114767));
	inittable_114822(initialsize_115373, (&(*result)));
	return result;
}

N_NIMCALL(NI, rawget_115093)(table114767 t_115100, NimStringDesc* key_115103) {
	NI result;
	NI h;
	NI LOC1;
	result = 0;
	LOC1 = 0;
	LOC1 = hash_98842(key_115103);
	h = (NI)(LOC1 & (t_115100.Data->Sup.len-1));
	{
		while (1) {
			if (!!((t_115100.Data->data[h].Field0 == ((NU8) 0)))) goto LA3;
			{
				NIM_BOOL LOC6;
				LOC6 = 0;
				LOC6 = eqStrings(t_115100.Data->data[h].Field1, key_115103);
				if (!(LOC6)) goto LA7;
				LOC6 = (t_115100.Data->data[h].Field0 == ((NU8) 1));
				LA7: ;
				if (!LOC6) goto LA8;
				result = h;
				goto BeforeRet;
			}
			LA8: ;
			h = nexttry_108409(h, (t_115100.Data->Sup.len-1));
		} LA3: ;
	}
	result = -1;
	BeforeRet: ;
	return result;
}

N_NIMCALL(void, rawinsert_115220)(table114767* t_115229, keyvaluepairseq114770** data_115237, NimStringDesc* key_115241, hmm114724* val_115243) {
	NI h;
	NI LOC1;
	NimStringDesc* LOC4;
	LOC1 = 0;
	LOC1 = hash_98842(key_115241);
	h = (NI)(LOC1 & ((*data_115237)->Sup.len-1));
	{
		while (1) {
			if (!((*data_115237)->data[h].Field0 == ((NU8) 1))) goto LA3;
			h = nexttry_108409(h, ((*data_115237)->Sup.len-1));
		} LA3: ;
	}
	LOC4 = 0;
	LOC4 = (*data_115237)->data[h].Field1; (*data_115237)->data[h].Field1 = copyStringRC1(key_115241);
	if (LOC4) nimGCunrefNoCycle(LOC4);
	genericSeqAssign((&(*data_115237)->data[h].Field2), val_115243, (&NTI114724));
	(*data_115237)->data[h].Field0 = ((NU8) 1);
}

N_NIMCALL(void, enlarge_115150)(table114767* t_115159) {
	keyvaluepairseq114770* n;
	keyvaluepairseq114770* LOC8;
	n = 0;
	n = (keyvaluepairseq114770*) newSeq((&NTI114770), (NI64)((*t_115159).Data->Sup.len * 2));
	{
		NI i_115205;
		NI HEX3Atmp_115284;
		NI res_115287;
		i_115205 = 0;
		HEX3Atmp_115284 = 0;
		HEX3Atmp_115284 = ((*t_115159).Data->Sup.len-1);
		res_115287 = 0;
		{
			while (1) {
				if (!(res_115287 <= HEX3Atmp_115284)) goto LA3;
				i_115205 = res_115287;
				{
					if (!((*t_115159).Data->data[i_115205].Field0 == ((NU8) 1))) goto LA6;
					rawinsert_115220(t_115159, (&n), (*t_115159).Data->data[i_115205].Field1, (*t_115159).Data->data[i_115205].Field2);
				}
				LA6: ;
				res_115287 += 1;
			} LA3: ;
		}
	}
	LOC8 = 0;
	LOC8 = (*t_115159).Data;
	unsureAsgnRef((void**) (&(*t_115159).Data), n);
	n = LOC8;
}

N_NIMCALL(void, HEX5BHEX5DHEX3D_115075)(table114767* t_115084, NimStringDesc* key_115088, hmm114724* val_115090) {
	NI index;
	index = rawget_115093((*t_115084), key_115088);
	{
		if (!(0 <= index)) goto LA3;
		genericSeqAssign((&(*t_115084).Data->data[index].Field2), val_115090, (&NTI114724));
	}
	goto LA1;
	LA3: ;
	{
		{
			NIM_BOOL LOC8;
			LOC8 = 0;
			LOC8 = mustrehash_108302((*t_115084).Data->Sup.len, (*t_115084).Counter);
			if (!LOC8) goto LA9;
			enlarge_115150(t_115084);
		}
		LA9: ;
		rawinsert_115220(t_115084, (&(*t_115084).Data), key_115088, val_115090);
		(*t_115084).Counter += 1;
	}
	LA1: ;
}

N_NIMCALL(void, HEX5BHEX5DHEX3D_116965)(table114767* t_116972, NimStringDesc* key_116975, hmm114724* val_116977) {
	HEX5BHEX5DHEX3D_115075(t_116972, key_116975, val_116977);
}

N_NIMCALL(hmm114724*, HEX5BHEX5D_117461)(table114767 t_117468, NimStringDesc* key_117471) {
	hmm114724* result;
	NI index;
	result = 0;
	index = rawget_115093(t_117468, key_117471);
	{
		if (!(0 <= index)) goto LA3;
		genericSeqAssign((&result), t_117468.Data->data[index].Field2, (&NTI114724));
	}
	LA3: ;
	return result;
}

N_NIMCALL(hmm114724*, HEX5BHEX5D_117447)(table114767* t_117454, NimStringDesc* key_117457) {
	hmm114724* result;
	result = 0;
	result = HEX5BHEX5D_117461((*t_117454), key_117457);
	return result;
}
NIM_EXTERNC N_NOINLINE(void, HEX00_tablesInit)(void) {
}

NIM_EXTERNC N_NOINLINE(void, HEX00_tablesDatInit)(void) {
static TNimNode* TMP55[2];
static TNimNode* TMP56[3];
static TNimNode* TMP57[3];
NI TMP59;
static char* NIM_CONST TMP58[3] = {
"seEmpty", 
"seFilled", 
"seDeleted"};
static TNimNode* TMP63[2];
static TNimNode* TMP64[3];
static TNimNode TMP5[18];
NTI113814.size = sizeof(table113814);
NTI113814.kind = 18;
NTI113814.base = 0;
NTI113814.flags = 2;
TMP55[0] = &TMP5[1];
NTI113820.size = sizeof(keyvaluepair113820);
NTI113820.kind = 18;
NTI113820.base = 0;
NTI113820.flags = 2;
TMP56[0] = &TMP5[3];
NTI108009.size = sizeof(NU8);
NTI108009.kind = 14;
NTI108009.base = 0;
NTI108009.flags = 3;
for (TMP59 = 0; TMP59 < 3; TMP59++) {
TMP5[TMP59+4].kind = 1;
TMP5[TMP59+4].offset = TMP59;
TMP5[TMP59+4].name = TMP58[TMP59];
TMP57[TMP59] = &TMP5[TMP59+4];
}
TMP5[7].len = 3; TMP5[7].kind = 2; TMP5[7].sons = &TMP57[0];
NTI108009.node = &TMP5[7];
TMP5[3].kind = 1;
TMP5[3].offset = offsetof(keyvaluepair113820, Field0);
TMP5[3].typ = (&NTI108009);
TMP5[3].name = "Field0";
TMP56[1] = &TMP5[8];
TMP5[8].kind = 1;
TMP5[8].offset = offsetof(keyvaluepair113820, Field1);
TMP5[8].typ = (&NTI149);
TMP5[8].name = "Field1";
TMP56[2] = &TMP5[9];
TMP5[9].kind = 1;
TMP5[9].offset = offsetof(keyvaluepair113820, Field2);
TMP5[9].typ = (&NTI113639);
TMP5[9].name = "Field2";
TMP5[2].len = 3; TMP5[2].kind = 2; TMP5[2].sons = &TMP56[0];
NTI113820.node = &TMP5[2];
NTI113817.size = sizeof(keyvaluepairseq113817*);
NTI113817.kind = 24;
NTI113817.base = (&NTI113820);
NTI113817.flags = 2;
NTI113817.marker = TMP60;
TMP5[1].kind = 1;
TMP5[1].offset = offsetof(table113814, Data);
TMP5[1].typ = (&NTI113817);
TMP5[1].name = "data";
TMP55[1] = &TMP5[10];
TMP5[10].kind = 1;
TMP5[10].offset = offsetof(table113814, Counter);
TMP5[10].typ = (&NTI108);
TMP5[10].name = "counter";
TMP5[0].len = 2; TMP5[0].kind = 2; TMP5[0].sons = &TMP55[0];
NTI113814.node = &TMP5[0];
NTI113811.size = sizeof(table113814*);
NTI113811.kind = 22;
NTI113811.base = (&NTI113814);
NTI113811.flags = 2;
NTI113811.marker = TMP61;
NTI114767.size = sizeof(table114767);
NTI114767.kind = 18;
NTI114767.base = 0;
NTI114767.flags = 2;
TMP63[0] = &TMP5[12];
NTI114773.size = sizeof(keyvaluepair114773);
NTI114773.kind = 18;
NTI114773.base = 0;
NTI114773.flags = 2;
TMP64[0] = &TMP5[14];
TMP5[14].kind = 1;
TMP5[14].offset = offsetof(keyvaluepair114773, Field0);
TMP5[14].typ = (&NTI108009);
TMP5[14].name = "Field0";
TMP64[1] = &TMP5[15];
TMP5[15].kind = 1;
TMP5[15].offset = offsetof(keyvaluepair114773, Field1);
TMP5[15].typ = (&NTI149);
TMP5[15].name = "Field1";
TMP64[2] = &TMP5[16];
TMP5[16].kind = 1;
TMP5[16].offset = offsetof(keyvaluepair114773, Field2);
TMP5[16].typ = (&NTI114724);
TMP5[16].name = "Field2";
TMP5[13].len = 3; TMP5[13].kind = 2; TMP5[13].sons = &TMP64[0];
NTI114773.node = &TMP5[13];
NTI114770.size = sizeof(keyvaluepairseq114770*);
NTI114770.kind = 24;
NTI114770.base = (&NTI114773);
NTI114770.flags = 2;
NTI114770.marker = TMP69;
TMP5[12].kind = 1;
TMP5[12].offset = offsetof(table114767, Data);
TMP5[12].typ = (&NTI114770);
TMP5[12].name = "data";
TMP63[1] = &TMP5[17];
TMP5[17].kind = 1;
TMP5[17].offset = offsetof(table114767, Counter);
TMP5[17].typ = (&NTI108);
TMP5[17].name = "counter";
TMP5[11].len = 2; TMP5[11].kind = 2; TMP5[11].sons = &TMP63[0];
NTI114767.node = &TMP5[11];
NTI114764.size = sizeof(table114767*);
NTI114764.kind = 22;
NTI114764.base = (&NTI114767);
NTI114764.flags = 2;
NTI114764.marker = TMP70;
}

