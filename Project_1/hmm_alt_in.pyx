from __future__ import division
from collections import defaultdict
import random
from itertools import product
import math
import numpy as np
cimport numpy as np

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t	

import matplotlib.pyplot as plt
from collections import Counter

### Initially modelled using a 2 state fully connected HMM ####

cdef class Trellis(object):

	#Defining the transition probabilities as class variables so
	#values remain the same
	cdef object p_t  #This is the transition probability
	cdef object q_ys   #This the state output probability
	cdef object train_log_prob_list #This is used to store every iteration's log probability values of train data
	cdef object test_log_prob_list #This is used to plot every iteration's test log probability

	def __init__(self):
		"""
		The transition probabilities are defined as dictionaries
		"""
		cdef p_t = <dict> defaultdict(float)
		self.p_t = p_t
		cdef q_ys = <dict> defaultdict(float)
		self.q_ys = q_ys
		self.train_log_prob_list = []
		self.test_log_prob_list = []

	def initialize(self, t_probs, obs_seq):
		"""
		Initialize the probabilities based on relative frequencies
		"""
		iter_count = 0
		for i, j in product([1,2], [1,2]):
			self.p_t[(i,j)] = t_probs[iter_count]
			iter_count += 1

		relativeFrequencies = Counter(obs_seq)  #This returns a dictionary with counts of each letter in text A
		totalCount = len(obs_seq)
		p_lambda = 0.00001 	#Chosen from experiments on the data

		random_vector = np.random.rand(27) #Generates a vector of length 27
		random_vector = random_vector - np.mean(random_vector) #Subtract out the mean
		pertubation_vector = p_lambda * random_vector

		letter_freq = []
		for i in range(1, 28):
			if i == 27:
				letter_freq.append(relativeFrequencies[" "]) #Add space at the end
			else:
				letter_freq.append(relativeFrequencies[chr(i + 96)])

		#Calculate letter probs
		letter_count = np.array(letter_freq)
		letter_prob = letter_count/np.sum(letter_count)

		#Keep reducing lambda to ensure that values stay greater than 0
		while ((True in ((letter_prob - pertubation_vector) < 0)) and (True in ((letter_prob + pertubation_vector) < 0))):
			pertubation_vector = pertubation_vector * 0.5
		prob_s1 = letter_prob - pertubation_vector
		prob_s2 = letter_prob + pertubation_vector

		for i in range(1, 28):
			self.q_ys[(1, i)] = prob_s1[i-1]
			self.q_ys[(2, i)] = prob_s2[i-1]

	def pretty_print_t(self):
		p_t = self.p_t
		print "The transition probabilities are"
		for (i, j) in product([1,2], [1,2]):
			print "(s{0} --> s{1} : {2}".format(i, j, p_t[(i,j)])

	def pretty_print_q(self):
		"""
		Pretty prints the output emission probabilities
		"""
		q_ys = self.q_ys
		result_list_1 = []
		result_list_2 = []
		for (i, j) in product([1,2], range(1, 28)):
			letter = ""
			if j == 27:
				letter = "#"
			else:
				letter = chr(j + 96)
			prob = q_ys[(i, j)]
			if i == 1:
				result_list_1.append(prob)
			else:
				result_list_2.append(prob)
		print "The emission output probabilities are "
		for i, (val1, val2) in enumerate(zip(result_list_1, result_list_2)):
			letter = ""
			if i == 26:
				letter = "#"
			else:
				letter = chr(i+97)
				print "(s1, {0}) - {1} and (s2, {0}) - {2}".format(letter, val1, val2)

	cdef void estimate_prob(self, obs_seq):
		"""
		Method that runs forward backward and does
		Baum-Welch re-estimation
		"""
		cdef p_t = <dict>defaultdict(float)
		p_t = self.p_t
		cdef q_ys = <dict>defaultdict(float)
		q_ys = self.q_ys
		train_log_prob_list = self.train_log_prob_list

		cdef alpha_hat = <dict>defaultdict(float)    #Alpha is indexed by (stage, state). i.e if in state s1 at the 10th stage, the key will be (10, 1)
		cdef beta_hat = <dict>defaultdict(float)    #Beta is indexed by (stage, state). Same as alpha
		cdef alpha_star = <dict>defaultdict(float)
		cdef beta_star = <dict>defaultdict(float)
		cdef norm_factor = []   #This is a list to store all the Q values used for scaling

		cdef count_t = <dict>defaultdict(float)
		cdef count_yt = <dict>defaultdict(float)


		state_list = range(1, 3)

		#It can start from either state with equal probability
		alpha_star[(0, 1)] = 0.5
		alpha_star[(0, 2)] = 0.5
		norm_factor.append(1)   #Q_0 is 1
		norm_append = norm_factor.append

		#Loop over the entire observation sequence
		cdef int i, final_stage_index
		cdef char *obs
		final_stage_index = len(obs_seq)
		cdef int stage_index, obs_index
		cdef int prev_state, cur_state, next_state
		cdef double left_total, right_total, p_star_i

		for i, obs in enumerate(obs_seq):
			stage_index = i+1  #Stages start from 1
			obs_index = 27   #This is the index value of space represented by #
			if <bytes>obs != " ":
				obs_index = ord(obs) - 96  #This way a will be 1, b will be 2, c will be 3 and so on
			current_q = 0.0
			for cur_state in state_list: #Over each state
				temp_alpha_hat = 0.0
				for prev_state in state_list:
					temp_alpha_hat += alpha_star[(stage_index - 1, prev_state)] * p_t[(prev_state, cur_state)] * q_ys[(cur_state, obs_index)]
				alpha_hat[(stage_index, cur_state)] = temp_alpha_hat
				current_q += temp_alpha_hat
			for cur_state in state_list:
				alpha_star[(stage_index, cur_state)] = alpha_hat[(stage_index, cur_state)]/current_q
			norm_factor.append(current_q)  #Store this value of normalizing factor

		####### Calculate backward stage probabilities #############

		##### First initialize final stage betas
		for cur_state in state_list:
			beta_star[(final_stage_index, cur_state)] = 1.0

		for i, obs in enumerate(reversed(obs_seq)): #Reversing the string and iterating backwards
			stage_index = final_stage_index - i - 1
			obs_index = 27
			if <bytes> obs != " ":
				obs_index = ord(obs) - 96  #This way a will be 1, b will be 2, c will be 3 and so on
			#current_q = 0.0
			for cur_state in state_list: #Over each state
				temp_beta_hat = 0.0
				for next_state in state_list:
					temp_beta_hat += beta_star[(stage_index + 1, next_state)] * p_t[(cur_state, next_state)] * q_ys[(next_state, obs_index)]
				beta_hat[(stage_index, cur_state)] = temp_beta_hat
				current_q += temp_beta_hat
			for cur_state in state_list:
				beta_star[(stage_index, cur_state)] = beta_hat[(stage_index, cur_state)]/norm_factor[stage_index]

		##### Calculate log probability ##########
		norm_array = np.array(norm_factor)   #Store this in a matrix for faster calculation. No looping required
		log_norm = np.log(norm_array)
		log_prob = np.sum(log_norm)/final_stage_index # Sum of log q's is the log probability
		train_log_prob_list.append(log_prob)

		##### Re-estimate probabilities ##########
		for i, obs in enumerate(obs_seq):
			stage_index = i # P* calculation starts from i=0
			obs_index = 27
			if <bytes> obs != " ":
				obs_index = ord(obs) - 96  #This way a will be 1, b will be 2, c will be 3 and so on
			for cur_state in state_list:
				for next_state in state_list:
					p_star_i = alpha_star[(stage_index, cur_state)]*p_t[(cur_state, next_state)] * q_ys[(next_state, obs_index)] * beta_star[(stage_index + 1, next_state)]
					count_t[(cur_state, next_state)] += p_star_i
					count_yt[(next_state, obs_index)] += p_star_i
		
		#Now re-calculate the probabilities
		for prev_state in state_list:
			left_total = 0.0
			for cur_state in state_list:
				left_total += count_t[(prev_state, cur_state)]
			for cur_state in state_list:
				p_t[(prev_state, cur_state)] = count_t[(prev_state, cur_state)]/left_total

		for obs_index in range(1, 28):
			for cur_state in state_list:
				right_total = 0.0
				for prev_state in state_list:
					right_total += count_t[(prev_state, cur_state)]
				q_ys[(cur_state, obs_index)] = count_yt[(cur_state, obs_index)]/right_total

	def calculate_likelihood(self, obs_seq):
		"""
		Calculates the likelihood of the test sequence with one run of the forward algorithm
		Code is duplicated from the previous method :(
		"""
		p_t = self.p_t
		q_ys = self.q_ys
		test_log_prob_list = self.test_log_prob_list

		alpha_hat = defaultdict(float)    #Alpha is indexed by (stage, state). i.e if in state s1 at the 10th stage, the key will be (10, 1)
		alpha_star = defaultdict(float)
		norm_factor = []   #This is a list to store all the Q values used for scaling

		state_list = range(1, 3)

		#It can start from either state with equal probability
		alpha_star[(0, 1)] = 0.5
		alpha_star[(0, 2)] = 0.5
		norm_factor.append(1)   #Q_0 is 1

		#Loop over the entire observation sequence
		final_stage_index = len(obs_seq)
		for i, obs in enumerate(obs_seq):
			stage_index = i+1  #Stages start from 1
			obs_index = 27   #This is the index value of space represented by #
			if obs != " ":
				obs_index = ord(obs) - 96  #This way a will be 1, b will be 2, c will be 3 and so on
			current_q = 0.0
			for cur_state in state_list: #Over each state
				temp_alpha_hat = 0.0
				for prev_state in state_list:
					temp_alpha_hat += alpha_star[(stage_index - 1, prev_state)] * p_t[(prev_state, cur_state)] * q_ys[(cur_state, obs_index)]
				alpha_hat[(stage_index, cur_state)] = temp_alpha_hat
				current_q += temp_alpha_hat
			for cur_state in state_list:
				alpha_star[(stage_index, cur_state)] = alpha_hat[(stage_index, cur_state)]/current_q
			norm_factor.append(current_q)  #Store this value of normalizing factor

		##### Calculate log probability ##########
		norm_array = np.array(norm_factor)   #Store this in a matrix for faster calculation. No looping required
		log_norm = np.log(norm_array)
		log_prob = np.sum(log_norm)/final_stage_index # Sum of log q's is the log probability
		test_log_prob_list.append(log_prob)


def main():
	#Read the file and extract the observation sequence
	obs_seq = ""
	with open("textA.txt") as f:
		obs_seq = f.readline()

	test_seq = ""
	with open("textB.txt") as f:
		test_seq = f.readline()

	num_iter = 600  #Number of EM iterations
	trellis = Trellis()

	#### Initialize the probabilities according to method suggested #####
	t_probs = [0.49, 0.51, 0.51, 0.49]  #p(1/1), p(2/1), p(1,2), p(2, 2)
	trellis.initialize(t_probs, obs_seq)

	letter_a_emission_s1 = []
	letter_a_emission_s2 = []
	letter_n_emission_s1 = []
	letter_n_emission_s2 = []
	letter_e_emission_s1 = []
	letter_e_emission_s2 = []

	##### Now estimate the new probabilities #####
	for iteration in range(num_iter):
		trellis.estimate_prob(obs_seq)

		####### Update the emission probabilities for few letters #########
		letter_a_emission_s1.append(trellis.q_ys[(1, 1)]) #for a
		letter_a_emission_s2.append(trellis.q_ys[(2, 1)]) #for a
		letter_e_emission_s1.append(trellis.q_ys[(1, 5)]) #for e
		letter_e_emission_s2.append(trellis.q_ys[(2, 5)]) #for e
		letter_n_emission_s1.append(trellis.q_ys[(1, 14)]) #for n
		letter_n_emission_s2.append(trellis.q_ys[(2, 14)]) #for n

		trellis.calculate_likelihood(test_seq)

	train_log_probs = trellis.train_log_prob_list
	train_log_prob_array = np.array(train_log_probs)
	test_log_probs = trellis.test_log_prob_list
	test_log_prob_array = np.array(test_log_probs)

	plt.figure(0)
	plt.plot(train_log_prob_array, label="Training data")
	plt.plot(test_log_prob_array, label = "Test data")
	plt.xlabel("Number of iterations")
	plt.ylabel("Average Log Probability")
	plt.legend(loc = 4)

	plt.figure(1)
	plt.plot(np.array(letter_a_emission_s1), label = "Letter a by state s1")
	plt.plot(np.array(letter_a_emission_s2), label = "Letter a by state s2")
	plt.plot(np.array(letter_e_emission_s1), label = "Letter e by state s1")
	plt.plot(np.array(letter_e_emission_s2), label = "Letter e by state s2")
	plt.plot(np.array(letter_n_emission_s1), label = "Letter n by state s1")
	plt.plot(np.array(letter_n_emission_s2), label = "Letter n by state s2")
	plt.xlabel("Number of iterations")
	plt.ylabel("Letter emission Probability")
	plt.legend(loc = 4)
	plt.ion()

	trellis.pretty_print_t()
	print
	trellis.pretty_print_q()

	plt.show(block = True)


if __name__ == '__main__':
	main()