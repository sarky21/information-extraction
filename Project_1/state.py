class State(object):

	def __init__(self):
		self._stage = None
		self._back_pointer = None

	@property
	def stage(self):
	    return self._stage
	@stage.setter
	def stage(self, value):
	    self._stage = value

	@property
	def back_pointer(self):
	    return self._back_pointer
	@back_pointer.setter
	def back_pointer(self, value):
	    self._back_pointer = value
	
	