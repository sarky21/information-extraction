\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage[margin=0.8in]{geometry}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\title{\textbf{A Review of Large-vocabulary Continuous-speech Recognition: A Summary}}
\author{Satya Prateek Bommaraju\\
		sbommar1@jhu.edu}
\date{}
\begin{document}

\maketitle

The paper is a broad description of the current state of Large-vocabulary Recognition (LVR) systems. The author gives an overview of the design and techniques used for LVR systems and then describes some of the issues that need to be addressed before it's ready for widespread use.

The underlying theory behind large vocabulary speech recognition systems rooted in statistical modelling. The central idea is that if we assume our input speech symbol is a sequence of acoustic vectors $ y_{1}, y_{2} .... y_{T} $ the system needs to identify the most probable word sequence $w_{1}, w_{2}, .... w_{n}$ that the speech represents. This can be done via Bayes' rule as
\[ \hat{W} = \argmax_{w} P(W|Y) = \argmax_{w} \frac{P(W)P(Y|W)}{P(Y)} \]
To calculate this LVR systems make use of a language model that gives the probability of any word sequence and an acoustic model that gives the probability of the input acoustic vectors given a particular word sequence. 

The first task in an LVR system though is to identify the sequence of acoustic vectors. The input speech which is a noisy, continuous audio signal is then processed to extract the features. The various processing steps are designed to satisfy the assumptions made in the acoustic model.

 The acoustic model used by most LVR systems is a Hidden Markov Model (HMM). Each word in speech is made up of smaller sub-units called phones and since these are fewer in number than the total possible number of words it is much easier to decompose each word into its phonemes and then model them as an HMM. An HMM is a statistical model where for an acoustic vector sequence $Y$ and some state sequence $X = x(1), x(2), ... x(T)$ the joint probability distribution can be factored as \[ P(Y,X|M) = a_{x(0),x(1)}\prod_{t=1}^{T}b_{x(t)}(y_{t})a_{x(t)x(t+1)} \] 
 where $a_{ij}$ is the transition probability from state $i$ to state $j$
 and $b_{j}y_{t})$ is the probability of outputting acoustic vector $y_{t}$ when reaching state $j$.
To identify $P(Y|M)$ we can simply marginalize over all values of the state sequence. A better way to do this however is via the Forward-Backward algorithm which is a recursive method of finding this probability. 

For the acoustic model we need not build one HMM for each phoneme but can build models for different contexts of phonemes such as one for each unique pair of left and right neighbours. This group of three phonemes is known as a triphone and it helps capture the contextual differences between the way different phonemes are pronounced. The problem with triphone models is that they increase the number of parameters that have to be trained. This along with the fact that training data means that various techniques have to be used to make inference feasible. One of those techniques that has led to a significant accuracy improvement is that of HMM state tying. The idea behind this technique is to tie together (make the respective parameters equal) states which are "acoustically indistinguishable". This allows data to be pooled across all states while having fewer parameters to estimate, thus improving the quality of the parameter estimations.

The next major component of LVR systems is the language model which is used to estimate the probability of a given word sequence. This can be decomposed via the chain sequence into the product of the probabilities of word $w_{k}$ being uttered given the previous word in the sequence. If we assume that the sequence is an Nth order Markov chain then the probability \[ P(w_{k} | W_{l}^{k-1} = P(w_{k}|W_{k-n+1}^{k-1}) \]
This assumption is not that unrealistic because these model the local dependencies in the language. More importantly however they simplify probability estimations because all one needs to do is look up the counts of the n-gram sequence in the training data. These counts cannot be used as it is however because of the problem of sparsity in the training data we can never observe all the possible word sequences. Thus some of the frequency counts will be zero rendering the entire word sequence probability to become zero. One way to solve this is to allot some of the probability mass of the word distributions to n-grams that have not been seen yet in a process known as discounting. The other way is known as backoff, in which if a particular n-gram has never been seen before then we revert simply to the probability of the n-1 gram times a scaling probability. N-gram models suffer from various deficiencies in that they cannot model long range dependencies in the sentence. Despite this their performance and relative simplicity means that they are prevalent in LVR systems.

With models for calculating $P(W)$ and $P(Y|W)$ we can calculate $P(W|Y)$. However to find the sequence $\hat{W}$ that maximizes this probability we need to search the space of possible word sequences and find the most likely one. This search can done as usual in either a depth-first manner or a breadth-first manner. Breadth-first searches are generally preferred because in this all optimal paths are explored in parallel. It's easy to understand the breath-first search by visualising a branching tree network consisting of all possible word sequences branching out from some start words. If we replace each word by the phoneme models that constitute it, then the total cost of a path is the sum of log probabilities of the individual state transitions across the models. This problem is equivalent to a token-passing or dynamic programming problem where a token that keeps track of path cost is moved along each time an input acoustic vector is observed. At each node only the best token at that node is kept and when the last state is reached the node with the lowest cost is the one that represents the most likely sequence. This algorithm (known as the viterbi algorithm) is guaranteed to find the most likely sequence but to speed it up in practice a pruning heuristic known as beam search is used.

All of these techniques have helped LVR systems achieve a low misclassification rate but experimental results have shown that per-speaker error rates vary greatly. The author uses this to raise a number of issues that must be handled before widespread usage of LVR applications becomes feasible. 

The first is speaker adaptation. Since the spectral qualities of everyone's speech varies and it requires a significant amount of training data to achieve speaker independence, speaker adaptation of acoustic models is a big advantage. This allows systems to be used out-of the box with the accuracy improving as it collects more training data from the speech of the user. 

Noise is an ever present factor in most communications especially over a channel. The presence of such noise alters the spectral characteristic of the speech signal and can make estimation a challenge. Methods to tackle this problem involve designing noise invariant features, noise removal or even use speaker adaptation techniques to adapt to the noise.

Language models are essentially constructed on the basis of n-gram frequencies and these often vary based on the nature of the speech domain. This causes problems when models trained on speech from one domain are forced to adapt to another. This is one of the limitations of the n-gram approach to language modelling and techniques that use statistical approaches to model the underlying grammar of the language is being tested to develop domain independence. Another approach is to use mixture models wherein each model is trained on different domain data.

Earlier speech recognition systems were trained on data that was recorded in a closed room with a good microphone and from speakers who read from a script. But casual speech involves a lot of complexities such as hesitations, interjections, stammering, stuttering and a lack of fluency. The author however notes that not much progress has been made in this area when it comes to LVR systems.

The last challenge described is the computational time required by LVR systems. To achieve nearly real time response to a user's speech, the process of hypothesis search has to be greatly sped up. This can be done via several tricks such as pruning, model approximations, lookahead and preselection of states by identifying those Gaussians that give a high likelihood.

In all the author concludes that while much work has to be done in order to perfect LVR systems, they are on the cusp of breakthrough into mainstream usage. Given a set of controlled conditions such as quiet environment or a well defined task domain, LVR systems work pretty well. He is optimistic however that in the future faster machines and access to more data will help improve the performance and speed of the system.\end{document}
