import tables, hashes
import os
import math
import strutils
import times
import nimprof

var t_0 = cpuTime()

#Initialize the global variables
type
    State = tuple[start_state: int, end_state:int]
    Emission_state = tuple[letter_output: int, end_state: int]
    Stage = tuple[stage: int, cur_state:int]
    Matrix = tuple[s1:seq[float], s2:seq[float], s3:seq[float], s4:seq[float]]

var
    p_t = newTable[State, float]()
    q_ys = newTable[Emission_state, float]()
    train_probs = newSeq[float](650)
    test_probs = newSeq[float](650)

#Read the training sequence from file
var a = open("textA.txt")
let obs_seq = a.readline()
a.close()

#Read the test sequence from file
var b = open("textB.txt")
let test_seq = b.readline()
b.close()

let num_iter: int = 600

var t_probs:Matrix = (@[0.22, 0.26, 0.26, 0.26],@[0.26, 0.22, 0.26, 0.26],@[0.26, 0.26, 0.22, 0.26],@[0.26, 0.26, 0.26, 0.22])
var s1_probs = @[0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 
                    0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0367]
var s2_probs = @[0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 0.0371, 
                   0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0370, 0.0367]
var s3_probs = @[0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 
                    0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.0367]
var s4_probs = @[0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 0.389, 
                   0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.350, 0.0367]


#Initialize the transition and emission probabilities
for i in @[0, 1, 2, 3]:
    p_t[(1, i+1)] = t_probs[0][i]

for i in @[0, 1, 2, 3]:
    p_t[(2, i+1)] = t_probs[1][i]

for i in @[0, 1, 2, 3]:
    p_t[(3, i+1)] = t_probs[2][i]

for i in @[0, 1, 2, 3]:
    p_t[(4, i+1)] = t_probs[3][i]

for letter_val, prob in s1_probs:
    q_ys[(letter_val+1, 1)] = prob

for letter_val, prob in s2_probs:
    q_ys[(letter_val+1, 2)] = prob

for letter_val, prob in s2_probs:
    q_ys[(letter_val+1, 3)] = prob

for letter_val, prob in s2_probs:
    q_ys[(letter_val+1, 4)] = prob


var
    letter_a_emission_s1, letter_a_emission_s2, letter_n_emission_s1, letter_n_emission_s2 : seq[float]

let printBool: bool = false

for iter in 1..num_iter:

    # Procedure that runs the whole gamut given an observation sequence
    proc full_estimation(obs_seq: string, train: bool) =
        var
            alpha_hat, alpha_star, beta_hat, beta_star = newTable[Stage, float]() #Indexed by (stage, state)
            count_t = newTable[State, float]() #Indexed by prevState, curState
            count_yt = newTable[Emission_state, float]()   #Indexed by letter_output, cur_state
            norm_seq = newSeq[float](len(obs_seq)+10)
            state_list = @[1, 2, 3, 4]

        #Initialize alpha values
        alpha_star[(0, 1)] = 0.25
        alpha_star[(0, 2)] = 0.25
        alpha_star[(0, 3)] = 0.25
        alpha_star[(0, 4)] = 0.25
        norm_seq.add(1.0)  #First norm factor is 0

        let len_obs: int = len(obs_seq)
        for i in 0..(len_obs-1):
            let obs = obs_seq[i]
            let stage_index = i+1 #Stage indices starts from 1
            var letter_index: int
            if $obs == " ":           #$ operator converts char to string
                letter_index = 27
            else:
                letter_index = ord(obs_seq[i]) - 96
            var current_q: float = 0.0

            for cur_state in state_list: #Over each state
                var temp_alpha_hat: float = 0.0
                for prev_state in state_list: 
                    ######### Should we check if prev_stage is there in alpha? ###########
                    temp_alpha_hat += alpha_star[(stage_index-1, prev_state)]*p_t[(prev_state, cur_state)]*q_ys[(letter_index, cur_state)]
                alpha_hat[(stage_index, cur_state)] = temp_alpha_hat
                current_q += temp_alpha_hat
            for cur_state in state_list:
                alpha_star[(stage_index, cur_state)] = alpha_hat[(stage_index, cur_state)]/current_q
            norm_seq.add(current_q)
        
        # Backward run
        beta_star[(len_obs, 1)] = 1.0
        beta_star[(len_obs, 2)] = 1.0
        beta_star[(len_obs, 3)] = 1.0
        beta_star[(len_obs, 4)] = 1.0

        for i in countdown(len_obs-1, 0):
            let obs = obs_seq[i]
            let stage_index = i #Stage indices starts from len_obs - 1
            var letter_index: int
            if $obs == " ":           #$ operator converts char to string
                letter_index = 27
            else:
                letter_index = ord(obs_seq[i]) - 96

            var current_q = 0.0
            for cur_state in state_list:
                var temp_beta_hat: float = 0.0
                for next_state in state_list:
                    temp_beta_hat += beta_star[(stage_index+1, next_state)]*p_t[(cur_state, next_state)]*q_ys[(letter_index, next_state)]
                beta_hat[(stage_index, cur_state)] = temp_beta_hat
                current_q += temp_beta_hat
            for cur_state in state_list:
                beta_star[(stage_index, cur_state)] = beta_hat[(stage_index, cur_state)]/norm_seq[i]

        
        #Calculate Log probabilities
        var log_prob_sum = 0.0
        for i, q_val in norm_seq:
            var log_q = ln(q_val)
            log_prob_sum += log_q
        var log_prob_avg:float = log_prob_sum/float(len_obs)
        if train:
            train_probs.add(log_prob_avg)
        else:
            test_probs.add(log_prob_avg)

        #Baum-welch re-estimation
        for i in countup(0, len_obs-1):
            let stage_index = i
            let obs = obs_seq[i]
            var letter_index: int
            if $obs == " ":           #$ operator converts char to string
                letter_index = 27
            else:
                letter_index = ord(obs_seq[i]) - 96

            for cur_state in state_list:
                for next_state in state_list:
                    var p_star_i: float
                    p_star_i = alpha_star[(stage_index, cur_state)]*p_t[(cur_state, next_state)]*q_ys[(letter_index, next_state)]*beta_star[(stage_index+1, next_state)]
                    if count_t.hasKey((cur_state, next_state)):
                        var temp_val:float = count_t[(cur_state, next_state)]
                        temp_val += p_star_i
                        count_t[(cur_state, next_state)] = temp_val
                    else:
                        count_t[(cur_state, next_state)] = p_star_i

                    if count_yt.hasKey((letter_index, next_state)):
                        var temp_val:float = count_yt[(letter_index, next_state)]
                        temp_val += p_star_i
                        count_yt[(letter_index, next_state)] = temp_val
                    else:
                        count_yt[(letter_index, next_state)] = p_star_i

        #Now recalculate the probabilities
        for prev_state in state_list:
            var left_total = 0.0
            for cur_state in state_list:
                left_total += count_t[(prev_state, cur_state)]
            for cur_state in state_list:
                p_t[(prev_state, cur_state)] = count_t[(prev_state, cur_state)]/left_total

        for letter_index in countup(1, 27):
            for cur_state in state_list:
                var right_total = 0.0
                for prev_state in state_list:
                    right_total += count_t[(prev_state, cur_state)]
                q_ys[(letter_index, cur_state)] = count_yt[(letter_index, cur_state)]/right_total



    full_estimation(obs_seq, true)
    full_estimation(test_seq, false)

echo "Finished in []s ", cpuTime() - t_0