import tables, hashes
import sequtils
import math
import strutils
import times
import algorithm

var t_0 = cpuTime()

################## Step 1 - Create elementary fenonic models ###########################

#Label names are a natural string sequence with ordering going from 0 to 255 defined by 
# 26*(ord(c1) - 65) + (ord(c2) - 65)

#Create a fenone type. This will simply be a tuple of probabilities
type
    fenone = tuple[p: seq[float], q: seq[seq[float]]]    #Each q is an array of length 255 

var fenone_list = newSeq[fenone](257)  #Hashmap that maps label to fenone

#Initialize all the fenone HMM's

for i in 0..255:
    var elementary_fenone: fenone
    elementary_fenone.p = @[0.8, 0.1, 0.1, 1.0, 1.0] #Last the 2 are incoming null at 1 and outgoing null at 2

    let q_prob = 0.5/255
    var temp_q = newSeqWith(2, newSeqWith(256, q_prob))

    #Set the actual output values as 0.5
    temp_q[0][i] = 0.5
    temp_q[1][i] = 0.5

    elementary_fenone.q = temp_q
    #Append the elementary fenone to fenone_list
    fenone_list[i] = elementary_fenone


####################### Step 2 - Create a silence model and append it to fenone_list ########################


var silence: fenone
silence.p = newSeqWith(14, 0.5)   #p1 to p9 are the output producing arcs. p10 to p12 are the null arcs. Last 2 are connecting nulls
silence.p[12] = 1.0    #Incoming null transition at 1
silence.p[13] = 1.0    #Outgoing null transition at 2
let sil_output_prob = 1/256
silence.q = newSeqWith(9, newSeqWith(256, sil_output_prob))
fenone_list[256] = silence

####################### Step 3 - Pick fenonic baseforms ########################

#Load the training script into memory
var scriptSeq:seq[string] = @[]

var f = open("data.2015/clsp.trnscr")

var count = 0
for line in f.lines:
    case count
    of 0: discard
    else:
        scriptSeq.add(line)
    count += 1
f.close()

#Define the baseform type. Baseform is just a list of integers referring to position of the fenones
type
    fenonic_baseform = seq[int]

#Load the endpoint information into a sequence
var endPtSeq = newSeqWith(798, @[0, 0])

f = open("data.2015/clsp.endpts")
count = 0
for line in f.lines:
    case count
    of 0: discard
    else:
        var silEnd:int = parseInt(line.split(" ")[0])  #End of the initial silence
        var silStart:int = parseInt(line.split(" ")[1])  #Start of the trailing silence
        endPtSeq[count-1] = @[silEnd, silStart]       
    count += 1
f.close()

#Load the training data, and identify the baseforms

var baseforms = newTable[string, fenonic_baseform]()  #create a string, HMM hashmap

var trnlbls:seq[string] = @[]

f = open("data.2015/clsp.trnlbls")
count = 0
for line in f.lines:
    case count
    of 0: discard
    else:
        var line_stripped = line.strip()
        trnlbls.add(line_stripped)
        var labels = line_stripped.split(" ") 
        var startPt:int = endPtSeq[count-1][0]
        var endPt:int = endPtSeq[count-1][1] - 2 #The file is 1 indexed and the value stored is the start of trailing silence
        var key:string = scriptSeq[count-1]

        var labels_length = endPt - startPt + 1

        #Only use first instance of word for baseform
        if baseforms.hasKey(key):
            count += 1
            continue
        else:
            var tempSeq = newSeq[int](labels_length)
            for i in startPt .. endPt:
                var lbl:string = labels[i]
                var lbl_index = 26*(ord(lbl[0]) - 65) + (ord(lbl[1]) - 65)
                tempSeq[i-startPt] = lbl_index
            baseforms[key] = tempSeq
    count += 1
f.close()

####################### Step 4 - Build word HMM's ########################

type
    arc = tuple[fenone_pos: int, p_pos: int, left_state:int, right_state:int]   #Fenone_pos is index of fenone_list. Q_pos is same as p_pos
    nArc = tuple[fenone_pos: int, p_pos: int, left_state:int, right_state:int]
    state = tuple[inNNull: seq[arc], inNull: seq[nArc], outNNull: seq[arc], outNull: seq[nArc]]
    hmm = seq[state]

var word_hmms = newTable[string, hmm]()

for word, baseform in baseforms.pairs:
    #Construct the HMM for this baseform
    var num_states = 7 + 2*len(baseform) + 7
    var temp_hmm = newSeq[state](num_states)

    ###### First add the leading silent state ###########
    temp_hmm[0] = (inNNull: @[], inNull: @[], outNNull: @[(256, 0, 0, 1), (256, 5, 0, 3)], outNull: @[])
    temp_hmm[1] = (inNNull: @[(256, 1, 1, 1), (256, 0, 0, 1)], inNull: @[], outNNull: @[(256, 1, 1, 1), (256, 2, 1, 2)], outNull: @[])
    temp_hmm[2] = (inNNull: @[(256, 2, 1, 2), (256, 3, 2, 2)], inNull: @[], outNNull: @[(256, 3, 2, 2), (256, 4, 2, 6)], outNull: @[])
    temp_hmm[3] = (inNNull: @[(256, 5, 0, 3)], inNull: @[], outNNull: @[(256, 6, 3, 4)], outNull: @[(256, 9, 3, 6)])
    temp_hmm[4] = (inNNull: @[(256, 6, 3, 4)], inNull: @[], outNNull: @[(256, 7, 4, 5)], outNull: @[(256, 10, 4, 6)])
    temp_hmm[5] = (inNNull: @[(256, 7, 4, 5)], inNull: @[], outNNull: @[(256, 8, 5, 6)], outNull: @[(256, 11, 5, 6)])
    temp_hmm[6] = (inNNull: @[(256, 4, 2, 6), (256, 8, 5, 6)], inNull: @[(256, 9, 3, 6), (256, 10, 4, 6),
                 (256, 11, 5, 6)], outNNull: @[], outNull: @[(256, 13, 6, 7)])    
    
    ###### Now add all the elementary hmm's ###########

    for i in countup(0, len(baseform) - 1):
        var fenone_index = baseform[i]
        var cur = 7 + 2*i
        temp_hmm[cur] = (inNNull: @[(fenone_index, 0, cur, cur)], inNull: @[(fenone_index, 3, cur-1, cur)], outNNull: @[(fenone_index, 0, cur, cur), 
                        (fenone_index, 1, cur, cur+1)], outNull: @[(fenone_index, 2, cur, cur+1)])
        temp_hmm[cur+1] = (inNNull: @[(fenone_index, 1, cur, cur+1)], inNull: @[(fenone_index, 2, cur, cur+1)],
                         outNNull: @[], outNull: @[(fenone_index, 4, cur+1, cur+2)])

    ###### Now add all the trailing silence hmm ###########
    var sil_start = 7 + 2*len(baseform)
    temp_hmm[sil_start + 0] = (inNNull: @[], inNull: @[(256, 12, sil_start-1, sil_start)],
                              outNNull: @[(256, 0, sil_start, sil_start +1), (256, 5, sil_start, sil_start + 3)], outNull: @[])
    temp_hmm[sil_start + 1] = (inNNull: @[(256, 1, sil_start + 1, sil_start + 1), (256, 0, sil_start, sil_start + 1)],
                              inNull: @[], outNNull: @[(256, 1, sil_start + 1, sil_start + 1), (256, 2, sil_start + 1, sil_start + 2)], outNull: @[])
    temp_hmm[sil_start + 2] = (inNNull: @[(256, 2, sil_start + 1, sil_start + 2), (256, 3, sil_start + 2, sil_start + 2)],
                              inNull: @[], outNNull: @[(256, 3, sil_start + 2, sil_start + 2), (256, 4, sil_start + 2, sil_start + 6)], outNull: @[])
    temp_hmm[sil_start + 3] = (inNNull: @[(256, 5, sil_start, sil_start + 3)], inNull: @[], 
                              outNNull: @[(256, 6, sil_start + 3, sil_start + 4)], outNull: @[(256, 9, sil_start + 3, sil_start + 6)])
    temp_hmm[sil_start + 4] = (inNNull: @[(256, 6, sil_start + 3, sil_start + 4)], inNull: @[], 
                              outNNull: @[(256, 7, sil_start + 4, sil_start + 5)], outNull: @[(256, 10, sil_start + 4, sil_start + 6)])
    temp_hmm[sil_start + 5] = (inNNull: @[(256, 7, sil_start + 4, sil_start + 5)], inNull: @[], 
                              outNNull: @[(256, 8, sil_start + 5, sil_start + 6)], outNull: @[(256, 11, sil_start + 5, sil_start + 6)])
    temp_hmm[sil_start + 6] = (inNNull: @[(256, 4, sil_start + 2, sil_start + 6), (256, 8, sil_start + 5, sil_start + 6)],
                              inNull: @[(256, 9, sil_start + 3, sil_start + 6), (256, 10, sil_start + 4, sil_start + 6),
                              (256, 11, sil_start + 5, sil_start + 6)], outNNull: @[], outNull: @[])

    word_hmms[word] = temp_hmm #Add this to the HMM list


####################### Step 5 - Training the word HMM's ########################

#### TODO - See if you will need to sort the training labels

var num_iters = 1 ######### Number of iterations 

#For each iteration do forward-backward over each utterance and then update probabilities


for iter in 1 .. num_iters:

    ############### Define the count variables ###################

    var count_list: seq[fenone]      #This is an array of counts for each fenone.

    count_list = newSeq[fenone](257)        

    for i in 0..255:
        var counts: fenone
        counts.p = @[0.0, 0.0, 0.0, 0.0, 0.0] #Last the 2 are incoming null at 1 and outgoing null at 2

        let q_prob = 0.0
        var temp_q = newSeqWith(2, newSeqWith(256, q_prob))

        counts.q = temp_q
        #Append the elementary fenone to fenone_list
        count_list[i] = counts

    var count_sil: fenone
    count_sil.p = newSeqWith(14, 0.0)   #p1 to p9 are the output producing arcs. p10 to p12 are the null arcs. Last 2 are connecting nulls
    count_sil.q = newSeqWith(9, newSeqWith(256, 0.0))
    count_list[256] = count_sil


    for sample_num, sample in trnlbls:
        var key = scriptSeq[sample_num]
        var hmm = word_hmms[key]

        var labels = sample.split(" ")
        var obs_len = len(labels)

        ##################### Forward backward over the joint HMM ###################

        var num_states = len(hmm)

        var alpha_hat = newSeqWith(obs_len+1, newSeqWith(num_states, 0.0))
        var alpha_star = newSeqWith(obs_len+1, newSeqWith(num_states, 0.0))
        var norm_seq = newSeq[float](obs_len+1)

        alpha_star[0][0] = 1.0 #Start state is always the first state

        norm_seq[0] = 1.0

        for stage, obs in labels:
            var obs_index = 26*(ord(obs[0]) - 65) + (ord(obs[1]) - 65)
            var stage_index = stage + 1
            var current_q = 0.0
            var temp_alpha_hat:float

            #Iterate over all states.
            for state_index in 0 .. < len(hmm):
                var cur_state = hmm[state_index]
                temp_alpha_hat = 0.0
                #Previous state incoming output arcs
                if cur_state.inNNull.len > 0: 
                    for inArc in cur_state.inNNull:
                        var fenone_index = inArc.fenone_pos
                        var arc_index = inArc.p_pos
                        var prev_state = inArc.left_state
                        var elem_fenone = fenone_list[fenone_index]
                        temp_alpha_hat += alpha_star[stage_index-1][prev_state]*elem_fenone.p[arc_index]*elem_fenone.q[arc_index][obs_index]

                if cur_state.inNull.len > 0:
                    for inArc in cur_state.inNull:
                        var fenone_index = inArc.fenone_pos
                        var arc_index = inArc.p_pos
                        var prev_state = inArc.left_state
                        var elem_fenone = fenone_list[fenone_index]
                        temp_alpha_hat += alpha_hat[stage_index][prev_state]*elem_fenone.p[arc_index]
                current_q += temp_alpha_hat
                alpha_hat[stage_index][state_index] = temp_alpha_hat

            #Normalize the stage alphas
            for state_index in 0 .. < len(hmm):
                alpha_star[stage_index][state_index] = alpha_hat[stage_index][state_index]/current_q
            norm_seq[stage_index] = current_q

        ################# Backward run ####################
        var reverseLabels:seq[string] = reversed(labels)

        var beta_hat = newSeqWith(obs_len+1, newSeqWith(num_states, 0.0))
        var beta_star = newSeqWith(obs_len+1, newSeqWith(num_states, 0.0))

        discard """
        for state in 0 .. < len(hmm):
            beta_star[obs_len][state] = 1.0"""

        beta_star[obs_len][num_states-1] = 1.0 # Final state is state 7 of the silence HMM



        for i, obs in reverseLabels:
            var obs_index = 26*(ord(obs[0]) - 65) + (ord(obs[1]) - 65)
            var stage_index = obs_len - i -1
            var temp_beta_hat:float

            #To handle null transitions, iterate backwards
            for state_index in countdown(hmm.len - 1, 0):
                var cur_state = hmm[state_index]
                temp_beta_hat = 0.0
                #Outgoing arcs to the next state
                if cur_state.outNNull.len > 0:
                    for outArc in cur_state.outNNull:
                        var fenone_index = outArc.fenone_pos
                        var arc_index = outArc.p_pos
                        var next_state = outArc.right_state
                        var elem_fenone = fenone_list[fenone_index]
                        temp_beta_hat += beta_star[stage_index+1][next_state]*elem_fenone.p[arc_index]*elem_fenone.q[arc_index][obs_index]
                
                #Outgoing null arcs to next state
                if cur_state.outNull.len > 0:
                    for outArc in cur_state.outNull:
                        var fenone_index = outArc.fenone_pos
                        var arc_index = outArc.p_pos
                        var next_state = outArc.right_state
                        var elem_fenone = fenone_list[fenone_index]
                        temp_beta_hat += beta_hat[stage_index][next_state]*elem_fenone.p[arc_index]
                
                beta_hat[stage_index][state_index] = temp_beta_hat
                beta_star[stage_index][state_index] = temp_beta_hat/norm_seq[stage_index]

        ################ Calculate log-probability ########################

        ################ Update the counts ########################

        for stage_index, obs in labels:
            var obs_index = 26*(ord(obs[0]) - 65) + (ord(obs[1]) - 65)
            
            #Iterate over all states.
            for state_index in 0 .. < len(hmm):
                var cur_state = hmm[state_index]

                # Outgoing output arcs
                if cur_state.outNNull.len > 0:
                    for outArc in cur_state.outNNull:
                        var fenone_index = outArc.fenone_pos
                        var arc_index = outArc.p_pos
                        var next_state = outArc.right_state
                        var elem_fenone = fenone_list[fenone_index]

                        var p_star:float = alpha_star[stage_index][state_index] * elem_fenone.p[arc_index]*
                                            elem_fenone.q[arc_index][obs_index] * beta_star[stage_index+1][next_state]
                        #Update the counts
                        count_list[fenone_index].p[arc_index] += p_star
                        count_list[fenone_index].q[arc_index][obs_index] += p_star
                
                #Outgoing null arcs to next state
                if cur_state.outNull.len > 0:
                    for outArc in cur_state.outNull:
                        var fenone_index = outArc.fenone_pos
                        var arc_index = outArc.p_pos
                        var next_state = outArc.right_state
                        var elem_fenone = fenone_list[fenone_index]

                        var p_star:float = alpha_star[stage_index][state_index] * elem_fenone.p[arc_index]*
                                           beta_star[stage_index][next_state]

                        #Update the counts
                        count_list[fenone_index].p[arc_index] += p_star


    ############### Update the parameters ###################
    #First update the elementary fenones
    for i in 0 .. 255:
        var counts = count_list[i]

        var c_j: float = 0.0
        #For all transitions
        for j in 0 .. 2:
            c_j += counts.p[j]

        if c_j != 0.0:  #Not all fenones are used as baseforms
            #Update probs
            for j in 0 .. 2:
                fenone_list[i].p[j] = counts.p[j]/c_j
                if j != 2:
                    for y in 0 .. 255:
                        fenone_list[i].q[j][y] = counts.q[j][y]/counts.p[j]

    #Update the silence model probabilities
    var arc_left_list:seq[seq[int]] = @[@[0, 5], @[1, 2], @[2, 1], @[3, 2], @[4, 3], @[5, 1], 
                                        @[6, 9], @[7, 10], @[8, 11], @[9, 6], @[10, 7], @[11, 8]]

    var counts = count_list[256]
    for j in 0 .. 11:
        var c_j:float = 0.0
        var left_arcs = arc_left_list[j]
        for k in left_arcs:
            c_j += counts.p[k]

        if c_j != 0.0:
            fenone_list[256].p[j] = counts.p[j]/c_j
            if j < 9:
                for y in 0..255:
                    fenone_list[256].q[j][y] = counts.q[j][y]/counts.p[j]    





echo "Finished in []s ", cpuTime() - t_0